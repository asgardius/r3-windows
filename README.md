# r3-windows

Windows Packaging template for The Red Robot Radio

[Microsoft Visual C++ Redistributable](https://docs.microsoft.com/en-US/cpp/windows/latest-supported-vc-redist?view=msvc-170) is required to run this game

# Packaging instructions

- Clone this repository
- Unpack [r3 repo](https://patrice.asgardius.company/gitea/asgardius/r3) content on this repo folder ("Source" and "level maps" folders are not required)
- Download [latest WinPython release](https://github.com/winpython/winpython), unpack it, copy python-x.y.z content on python folder inside this repo
- Open a command prompt inside python folder
- Run .\scripts\pip install pygame
- Adapt start.bat to python folder